# gentore_breeds

Last part of the GenTORE work on comparing resilience-related features of cows from different breeds and crossbreds.

> created by Ines Adriaens (adria036)
> created on March 21, 2022
> contributors: Gerbrich Bonekamp, Ines Adriaens

## Project aims

- Zoom in on the "herd" differences when looking at milk yield based sensor features of different breeds. 
- More specifically, we want to look whether there are differences across distributions and properties of farms with different breeds, of expected lactations, raw production values and when time left, DHI/MR data.
- Go beyond the 'intercept' effect of herd in the statistical models.
- This might give an idea of the identity of farms that choose for other breeds than HF cows.

### Alternative research trajectory

_(only when time)_  
- link a simplified resilience rank with with the resilience indicators (LnVAR, AC1) calculated in the past 


## Data description

- selection of dataset used in the past by Marieke Poppe - selection made by Gerbrich
-_more information will be added_ 


### try git add 

```
git status
git add .
git commit -m "this is my message"
git push origin main

```